import React from "react";
import style_desktop from "./desktop.module.scss";
import glass from "../../assets/purpose/glass.png";
import Image from "next/image";

const Purpose = () => {
  return (
    <div className={`${style_desktop.purpose} wrap`} id="purpose">
      <div className={style_desktop.image_glass}>
        <Image src={glass} alt="glass" />
      </div>
      <div className={style_desktop.content_purpose}>
        <p className={style_desktop.title_purpose}>Purpose of WINERY DAO</p>
        <p className={style_desktop.des_purpose}>
          We focus on providing decentralized financial services DeFi that can
          be accessed by anyone, anywhere. Our DeFi platform can be used for
          lending, staking, trading, liquidity mining, issuance, and more.
        </p>
      </div>
    </div>
  );
};

export default Purpose;
