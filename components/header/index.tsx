import Image from "next/image";
import React from "react";
import style_desktop from "./desktop.module.scss";
import logodesk from "../../assets/header/logo_header.png";

const Header = () => {
  return (
    <div className={style_desktop.header}>
      <div
        className={`wrap d-flex justify-content-center flex-column ${style_desktop.container}`}
      >
        <div className={`text-center ${style_desktop.logo}`}>
          <Image src={logodesk} alt="logo desk"></Image>
        </div>
        <h1 className={`text-center text-white ${style_desktop.title}`}>
          WINERY DAO
        </h1>
        <p className={`${style_desktop.content} text-white text-center`}>
          WINERY DAO is a decentralized platform available on the Binance Smart
          Chain based on the CORK token, creating a diverse ecosystem of NFT
          DeFi through a decentralized autonomous organization (DAO).
        </p>
        <div className={`${style_desktop.button} text-white text-center`} role="button">
          <p className={`${style_desktop.button_content} d-flex justify-content-center align-items-center`}>
            Read more
          </p>
        </div>
      </div>
    </div>
  );
};

export default Header;
