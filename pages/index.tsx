import type { NextPage } from "next";
import { Fragment } from "react";
import Header from "../components/header";
import Purpose from "../components/purpose";
import 'bootstrap/dist/css/bootstrap.min.css';

const Home: NextPage = () => {
  return (
    <Fragment>
      <Header />
      <Purpose />
    </Fragment>
  );
};

export default Home;
